import React, { Component } from 'react';
import { ScrollView } from 'react-native';

import Items from './Items';
import Axios from 'axios';

export default class ListaItems extends Component {

  constructor(props) {
    super(props);

    this.state = { listaItems: [] };
  }

  componentWillMount() {
    Axios.get('http://faus.com.br/recursos/c/dmairr/api/itens.html')
         .then(res => this.setState({ listaItems: res.data }))
         .catch(err => console.log(err));
  }

  render() {
    return (
      <ScrollView style={{backgroundColor: '#ddd'}}>
        {
          this.state.listaItems.map((item, index) => (
            <Items key={index} item={item}/>
          ))
        }
      </ScrollView>
    );
  }
}