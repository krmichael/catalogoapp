import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

export default class Items extends Component {
  render() {
    return (
      <View style={styles.item}>
        <View style={styles.foto}>
          <Image source={{uri: this.props.item.foto}} style={{width: 100, height: 100}}/>
        </View>
        <View style={styles.detalhesItem}>
          <Text style={styles.titulo}>{this.props.item.titulo}</Text>
          <Text style={styles.valor}>R$ {this.props.item.valor}</Text>
          <Text style={styles.detalhes}>Local: {this.props.item.local_anuncio}</Text>
          <Text style={styles.detalhes}>Publicação em: {this.props.item.data_publicacao}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#fff',
    elevation: 3,
    margin: 10,
    padding: 10,
    flexDirection: 'row'
  },
  foto: {
    width: 102,
    height: 102
  },
  detalhesItem: {
    marginLeft: 20,
    flex: 1
  },
  titulo: {
    fontSize: 18,
    color: '#0099FF',
    marginBottom: 5
  },
  valor: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  detalhes: {
    fontSize: 16
  }
});