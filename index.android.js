import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import ListaItems from './src/components/ListaItems';

class catalogoApp extends Component {
  render() {
    return (
      <ListaItems/>
    );
  }
}

AppRegistry.registerComponent('catalogoApp', () => catalogoApp);
